# Installation

* Clone Down the repo

* Install build tools dependencies (For instructions on installing NPM https://docs.npmjs.com/getting-started/installing-node) This installation has been tested on the following

* Npm V 4.1.2 & Node V v7.6.0

* Download and install all PHP dependencies (you need composer installed, which is outside the scope of this document)

````
$ composer install
````


````
$ npm install
````

* Copy the example environment file, and edit the configuration as required (mySQL Settings)
I have left the api key in the env.example file for ease of setup. I understand this is bad practice.
I have also added mail server details for the same reason. To change the notification email please look in the currencies table.
```
$ cp .env.example .env
```

````
$ php artisan key:generate
````

````
$ npm run production
````

#### Database

* Setting up tables and default data

Make sure you setup your database connection in the .env file before continuing
```
$ php artisan migrate && php artisan db:seed
```

#### Updating Rates

```
$ php artisan rates:update
```

#### Host File
* Create a host in your web server pointing to the ./public/ directory.
This is tested with Apache. Please follow this link for information on setting up
nginx site directives. https://laravel.com/docs/5.4/installation