@extends('layouts/main')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <h3>Order Confirmation Mail</h3>
            <table>
                <tr>
                    <td>Currency Purchased</td>
                    <td><?php echo $order->getOriginal('currency_to'); ?></td>
                </tr>
                <tr>
                    <td>Exchange Rate</td>
                    <td><?php echo $order->getOriginal('rate'); ?></td>
                </tr>
                <tr>
                    <td>Amount Purchased</td>
                    <td><?php echo $order->getOriginal('amount_bought'); ?></td>
                </tr>
                <tr>
                    <td>Surcharge</td>
                    <td><?php echo $order->getOriginal('surcharge'); ?></td>
                </tr>
                <tr>
                    <td>Discount</td>
                    <td><?php echo $order->getOriginal('discount'); ?></td>
                </tr>
                <tr>
                    <td>Total with Surcharge</td>
                    <td><?php echo $order->getOriginal('total'); ?></td>
                </tr>
                <tr>
                    <td>Grand Total</td>
                    <td><?php echo $order->getOriginal('grand_total'); ?></td>
                </tr>
            </table>
        </div>
    </div>
@endsection