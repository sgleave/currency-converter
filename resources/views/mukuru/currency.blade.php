@extends('layouts/main')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <h3>USD Converter</h3>

            <form class="form-horizontal converter" action="" method="POST">
                <div class="form-group">
                    <div class="col-sm-6">
                        <input type="text" id="usdAmount" name="usdAmount" class="form-control" value="1">
                    </div>
                    <div class="col-sm-6">
                        <select name="" class="form-control">
                            <option>USD</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <input type="text" id="forexAmount" name="forexAmount" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <select name="currency" class="form-control">
                            @foreach ($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->to }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table">
                        <tr>
                            <th>Surcharge</th>
                            <th>USD Total</th>
                        </tr>
                        <tr>
                            <td id="currency-surcharge"></td>
                            <td id="currency-total"></td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <div class=" col-sm-2">
                        <button type="submit" class="btn btn-default">Place Order</button>
                    </div>
                </div>
                <div class="form-group">
                    <h3 class="order-successful" style="display:none;">Order Successful</h3>
                </div>
            </form>
        </div>
    </div>
@endsection