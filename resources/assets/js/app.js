var current;
var finishedTyping;
function getConversion(conversionData) {
    jQuery.ajax({
        url: '/api/quote',
        method: 'POST',
        dataType: "json",
        data: conversionData,
        success: function (response) {
            current = response;
            jQuery('#currency-surcharge').html(response.surcharge);
            jQuery('#currency-total').html(response.total);

            jQuery('#usdAmount').val(response.amount_paid);
            jQuery('#forexAmount').val(response.amount_bought);
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
}
jQuery(document).ready(function () {
    var data = new Object();
    jQuery(jQuery('.converter').serializeArray()).each(function (index, obj) {
        data[obj.name] = obj.value;
    });
    getConversion(data);

    jQuery('.converter').on('submit', function (e) {
        e.preventDefault();
        jQuery('.order-successful').css('display','block');
        jQuery('.order-successful').html('Processing');
        jQuery.ajax({
            url: '/api/order',
            method: 'POST',
            dataType: "json",
            data: current,
            success: function (response) {
                if(response.status==true){
                    jQuery('.order-successful').css('display','block');
                    jQuery('.order-successful').html('Order Successful');
                }else{
                    jQuery('.order-successful').html('Order Unsuccesful');
                }
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    });

    jQuery('.converter select').on('change', function (e) {
        var data = new Object();
        var form = jQuery(this)[0].form;
        jQuery(jQuery(form).serializeArray()).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.usdAmount = 1;
        delete data.forexAmount;
        getConversion(data);
    });

    jQuery('.converter input[type=text]').on('keyup', function (e) {
        clearTimeout(finishedTyping);

        var code = (e.keyCode || e.which);
        if (code == 190) return true;

        var data = new Object();
        var form = jQuery(this)[0].form;

        jQuery(jQuery(form).serializeArray()).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        switch (jQuery(this).attr('id')) {
            case 'usdAmount' :
                delete data.forexAmount;
                break;
            case 'forexAmount' :
                delete data.usdAmount;
                break;
        }

        finishedTyping = setTimeout(function(){
            getConversion(data);
        },800);
    });

});