<?php
    
    use Illuminate\Database\Seeder;
    
    class currencies_seed extends Seeder
    {
        
        public function __construct()
        {
            // Clearing Table during testing of seeding.
            DB::table('currencies')->truncate();
        }
        
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $currencySeed = [
              [
                'from'         => 'USD',
                'to'           => 'ZAR',
                'rate'         => 13.3054,
                'surcharge'    => 7.5,
                'discount'     => 0,
                'notification' => '',
              ],
              [
                'from'         => 'USD',
                'to'           => 'GBP',
                'rate'         => 0.651178,
                'surcharge'    => 5,
                'discount'     => 0,
                'notification' => 'stephen.gleave@gmail.com',
              ],
              [
                'from'         => 'USD',
                'to'           => 'EUR',
                'rate'         => 0.884872,
                'surcharge'    => 5,
                'discount'     => 2,
                'notification' => '',
              ],
              [
                'from'         => 'USD',
                'to'           => 'KES',
                'rate'         => 103.860,
                'surcharge'    => 2.5,
                'discount'     => 0,
                'notification' => '',
              ],
            ];
            foreach ($currencySeed as $currency) {
                factory(App\Currency::class)->create($currency);
            }
            
        }
    }
