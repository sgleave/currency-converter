<?php
    
    
    
    $factory->define(
      App\Currency::class, function () {
        return [
          'from'         => 'USD',
          'to'           => 'ZAR',
          'rate'         => 1,
          'surcharge'    => 0,
          'discount'     => 0,
          'notification' => '',
        ];
    }
    );
