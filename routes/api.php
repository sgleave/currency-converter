<?php

use Illuminate\Http\Request;
    
     Route::post('quote', function (Request $request, \App\Http\Controllers\OrderController $orderController) {
        return response()->json($orderController->quote($request));
     });
    
    Route::post('order', function (Request $request, \App\Http\Controllers\OrderController $orderController) {
        return response()->json($orderController->order($request));
    });
