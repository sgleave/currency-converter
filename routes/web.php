<?php


Route::get('/', function (\App\Http\Controllers\CurrencyController $currencyController) {
    return $currencyController->show();
});