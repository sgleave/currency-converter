<?php
    
    namespace App\Console\Commands;
    
    use App\Currency;
    use Illuminate\Console\Command;
    use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
    
    class UpdateRates extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'rates:update';
        
        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Updates currency exchange rates';
        
        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
            
            
        }
        
        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $url = 'http://apilayer.net/api/live?access_key='.env('CURRENCYLAYER').'&source=USD&currencies=ZAR,GBP,EUR,KES&format = 1';
            $new = json_decode(file_get_contents($url), true);
            foreach ($new['quotes'] as $key => $val) {
                $currency = Currency::where('to', str_replace('USD', '', $key))->first();
                $currency->rate = $val;
                if($currency->save()){
                    $this->info("Successfully Updated ".str_replace('USD', '', $key));
                }else{
                    $this->info("There was a problem updating ".str_replace('USD', '', $key));
                }
                
                
            }
            
        }
    }
