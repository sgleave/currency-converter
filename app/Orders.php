<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    public $timestamps = true;
    
    protected $fillable = [
      'currency_from',
      'currency_to',
      'rate',
      'amount_paid',
      'amount_bought',
      'surcharge',
      'discount',
      'total',
      'grand_total',
    ];
}
