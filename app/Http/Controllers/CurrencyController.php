<?php
    namespace App\Http\Controllers;
    
    use App\Currency;
    
    class CurrencyController extends Controller
    {
        public function show()
        {
            return view('mukuru.currency', array('currencies' => Currency::all()));
        }
    }