<?php
    
    namespace App\Http\Controllers;
    
    use App\Currency;
    use App\Mail\OrderCreated;
    use App\Orders;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Mail;

    class OrderController extends Controller
    {
        public function quote(Request $request)
        {
            if (!$request->has('usdAmount') && !$request->has('forexAmount')) {
                return response()->json(
                  array(
                    'error'   => true,
                    'message' => 'Either amountToUSD or amountFromUSD fields are missing from request',
                  ), 400
                );
                exit;
            }
            if(!$request->input('currency')){
                return response()->json(
                  array(
                    'error'   => true,
                    'message' => 'Field currency is missing from request',
                  ), 400
                );
            }
            $currency = Currency::where('id', $request->input('currency'))->first();
            if ($request->has('usdAmount')) {
                return self::usdAmount($currency, $request->input('usdAmount'));
            } else {
                return self::forexAmount($currency, $request->input('forexAmount'));
            }
        }
        
        public static function order(Request $request){
            $order = new Orders();
            $order->fill($request->all());
            
            if($order->save()){
                $currency = Currency::where('to', $order->getOriginal('currency_to'))->first();
                if($currency->getOriginal('notification')){
                    Mail::to($currency->getOriginal('notification'))->send(new OrderCreated($order));
                }
                return array('status'=>true);
            }else{
                return array('status'=>false);
            }
        }
        
        //@todo clean up usdAmount & forexAmount works temporarily.
        public static function usdAmount(Currency $currency, $amount)
        {
            $amountbought = round($amount * $currency->getOriginal('rate'), 2);
            $surcharge = round($amount / 100 * $currency->getOriginal('surcharge'), 2);
            $total = round($amount + $surcharge, 2);
            $discount = round($total / 100 * $currency->getOriginal('discount'),2);
            
            $order = new Orders();
            $order->fill(
              array(
                'currency_from' => $currency->getOriginal('from'),
                'currency_to'   => $currency->getOriginal('to'),
                'rate'          => $currency->getOriginal('rate'),
                'amount_paid'   => round($amount, 2),
                'amount_bought' => $amountbought,
                'surcharge'     => $surcharge,
                'discount'      => $discount,
                'total'         => $total,
                'grand_total'   => $total-$discount
              )
            );
            
            return $order;
            
        }
        
        public static function forexAmount(Currency $currency, $amount)
        {
            $amountpaid = round($amount / $currency->getOriginal('rate'), 2);
            $surcharge = round($amountpaid / 100 * $currency->getOriginal('surcharge'), 2);
            $total = round($amountpaid + $surcharge, 2);
            $discount = round($total / 100 * $currency->getOriginal('discount'),2);
            
            $order = new Orders();
            $order->fill(
              array(
                'currency_from' => $currency->getOriginal('from'),
                'currency_to'   => $currency->getOriginal('to'),
                'rate'          => $currency->getOriginal('rate'),
                'amount_paid'   => $amountpaid,2,
                'amount_bought' => round($amount, 2),
                'surcharge'     => round($surcharge,2),
                'discount'      => $discount,
                'total'         => $total,
                'grand_total'   => $total-$discount
              )
            );
            return $order;
        }
    }
